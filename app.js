var app = angular.module('flapperNews', ['ui.router']);

app.config([
	'$stateProvider',
	'$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {

		$stateProvider
			.state('home', {
				url: '/home',
				templateUrl: '/home.html',
				controller: 'MainCtrl'
			})
		
			.state('posts', {
  				url: '/posts/{id}',
  				templateUrl: '/posts.html',
  				controller: 'PostsCtrl'
			})
		;

		$urlRouterProvider.otherwise('home');
	}]);

app.factory('posts', function(){
	var o = {
		posts: [
			{title: 'post 1', upvotes: 5, id: 0, comments: []},
			{title: 'post 2', upvotes: 2, id: 1, comments: []},
			{title: 'post 3', upvotes: 15, id: 2, comments: []},
			{title: 'post 4', upvotes: 9, id: 3, comments: []},
			{title: 'post 5', upvotes: 4, id: 4, comments: []}
		]
	};
	return o;
});

app.controller('MainCtrl', [
	'$scope', 
	'$timeout',
	'posts',
	function ($scope, $timeout, posts){

		$scope.posts = posts.posts;

		$scope.presentation = "Welcome!";
		


		$scope.addPost = function() {
			if (!$scope.tit || $scope.tit === '') {return;}
			$scope.posts.push({
				title: $scope.tit,
				link: $scope.link,
				upvotes: 0,
				id: $scope.posts.length, //id auto increase with each push
				comments: [
    				{author: 'Joe', body: 'Cool post!', upvotes: 0}, //just for demostration
    				{author: 'Bob', body: 'Great idea but everything is wrong!', upvotes: 0}
  				]

			});
    		$scope.tit = "";
    		$scope.link = "";
    	 };

    	 $scope.incrementUpvotes = function (p) {
    	 	p.upvotes += 1;
    	 }

	    $timeout(function() {
	    	$scope.presentation = "Create a post, upvote it!";
		}, 5000);

	}
]);

app.controller('PostsCtrl', [
	'$scope',
	'$stateParams',
	'posts',
	function($scope, $stateParams, posts){
		$scope.post = posts.posts[$stateParams.id];

		$scope.addComment = function(){
			if (!$scope.body || $scope.body === '') {return;}
			$scope.post.comments.push({
				author: 'user',
				body: $scope.body,
				upvotes: 0
			});
			$scope.body = '';
		};

		$scope.incrementUpvotes = function (p) {
    	 	p.upvotes += 1;
    	}
	}
]);
